﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvertisementsMVC.Models;
using Microsoft.EntityFrameworkCore;

namespace AdvertisementsMVC.Data
{
    public class MvcAdvertisementContext:DbContext
    {
        public DbSet<AdvertisementsMVC.Models.AdvertisementCard> AdvertisementCard { get; set; }
        public DbSet<AdvertisementsMVC.Models.User> Users { get; set; }

        public MvcAdvertisementContext(DbContextOptions<MvcAdvertisementContext> options)
            : base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=AdvertisementsMVC;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(@"Server=advertisementrazorview20200202080357dbserver.database.windows.net;Database=AdvertisementRazorView20200202080357_db;User Id=;Password=;Trusted_Connection=false;Encrypt=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdvertisementCard>()
                .HasOne(p => p.User)    //устанавливает навигационное свойство для сущности AdvertisementCard. Почему так важен выбор букв??
                .WithMany(t => t.AddedAdvertisements) //идентификация навигационного свойства на стороне связанной сущности 
                .HasForeignKey(p => p.UserId)
                //.HasPrincipalKey(p=>p.Name)
                .IsRequired();
        }
    }
}
