﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvertisementsMVC.Data;
using AdvertisementsMVC.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AdvertisementsMVC.Seeding
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcAdvertisementContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcAdvertisementContext>>()))
            {
                if (context.AdvertisementCard.Any())
                {
                    return;
                }

                for (int i = 0; i <= 500; i++)
                {
                    context.AdvertisementCard.AddRange(

                        new AdvertisementCard
                        {
                            Number = new Random().Next(10000),
                            Created = DateTime.Parse($"2020-{new Random().Next(1, 13)}-{new Random().Next(1, 30)}"),
                            AdvertisementText = Guid.NewGuid().ToString(),
                            Rating = new Random().Next(1, 11),
                            UserId = new Random().Next(1, 4)
                        }
                    );
                    context.SaveChanges();
                }
            }
        }
    }
}

