﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvertisementsMVC.Data;
using AdvertisementsMVC.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AdvertisementsMVC.Seeding
{
    public class SeedUser
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcAdvertisementContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcAdvertisementContext>>()))
            {
                if (context.Users.Any())
                {
                    return;
                }

                context.Users.AddRange(

                    new User
                    {
                        Name = "Tim",
                        LastName = "Burton",
                    }
                );
                context.Users.AddRange(
                    new User
                    {
                        Name = "Michael",
                        MiddleName = "Jeffery",
                        LastName = "Jordan",
                    }
                );
                context.Users.AddRange(
                    new User
                    {
                        Name = "Hanna",
                        LastName = "Montana",
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
