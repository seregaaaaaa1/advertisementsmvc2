﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AdvertisementsMVC.Data;
using AdvertisementsMVC.Models;

namespace AdvertisementsMVC.Controllers
{
    public class AdvertisementCardsController : Controller
    {
        private readonly MvcAdvertisementContext _context;

        public AdvertisementCardsController(MvcAdvertisementContext context)
        {
            _context = context;
        }

        // GET: AdvertisementCards
        public async Task<IActionResult> Index()
        {
            var mvcAdvertisementContext = _context.AdvertisementCard.Include(a => a.User);
            return View(await mvcAdvertisementContext.ToListAsync());
        }

        // GET: AdvertisementCards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var advertisementCard = await _context.AdvertisementCard
                .Include(a => a.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (advertisementCard == null)
            {
                return NotFound();
            }

            return View(advertisementCard);
        }

        // GET: AdvertisementCards/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = _context.Users.Select(x =>new SelectListItem
            {
                Text = $"{x.Name}   {x.LastName}",
                Value = x.Id.ToString()

            });//   new SelectList(_context.Users, "Id", "Name");
            return View();
        }

        // POST: AdvertisementCards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Number,Created,AdvertisementText,Rating,UserId")] AdvertisementCard advertisementCard)
        {
            if (ModelState.IsValid)
            {
                _context.Add(advertisementCard);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", advertisementCard.UserId);
            return View(advertisementCard);
        }

        // GET: AdvertisementCards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var advertisementCard = await _context.AdvertisementCard.FindAsync(id);
            if (advertisementCard == null)
            {
                return NotFound();
            }

            ViewData["UserId"] = _context.Users.Select(x => new SelectListItem
            {
                Text = $"{x.Name}   {x.LastName}",
                Value = x.Id.ToString()
            });
            return View(advertisementCard);
        }

        // POST: AdvertisementCards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Number,Created,AdvertisementText,Rating,UserId")] AdvertisementCard advertisementCard)
        {
            if (id != advertisementCard.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(advertisementCard);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdvertisementCardExists(advertisementCard.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", advertisementCard.UserId);
            return View(advertisementCard);
        }

        // GET: AdvertisementCards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var advertisementCard = await _context.AdvertisementCard
                .Include(a => a.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (advertisementCard == null)
            {
                return NotFound();
            }

            return View(advertisementCard);
        }

        // POST: AdvertisementCards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var advertisementCard = await _context.AdvertisementCard.FindAsync(id);
            _context.AdvertisementCard.Remove(advertisementCard);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AdvertisementCardExists(int id)
        {
            return _context.AdvertisementCard.Any(e => e.Id == id);
        }
    }
}
