﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvertisementsMVC.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Adress { get; set; }

        public string City { get; set; }


        public ICollection<AdvertisementCard> AddedAdvertisements { get; set; }

    }
}
