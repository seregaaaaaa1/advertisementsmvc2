﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdvertisementsMVC.Models
{
    public class AdvertisementCard
    {
        public int Id { get; set; }

        [Display(Name = "Номер")] //Number=Id из БД
        public int Number { get; set; }
        //{
        //    get { return Number; }
        //    set
        //    {
        //        if (value < 0)
        //        {
        //            throw new ArgumentException("Номер должен быть положительным");
        //        }
        //        Number = value;
        //    }
        //}
        [Display(Name = "Создано")]
        [DataType(DataType.DateTime)]
        public DateTime Created { get; set; }

        [Display(Name = "Объявление")]
        [Required(ErrorMessage = "Добавьте текст объявления")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string AdvertisementText { get; set; }

        [Display(Name = "Рейтинг")]
        public int Rating { get; set; }
        [Display(Name = "Пользователь")]
        public int UserId { get; set; } //внешний ключ
        public string Author { get; set; } //principalkey
        public User User { get; set; }       //навигационное свойство

    }
}
